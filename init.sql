DROP DATABASE IF EXISTS venteArme;
CREATE DATABASE venteArme;
USE venteArme;

DROP USER IF EXISTS armeUser@'127.0.0.1';
CREATE USER armeUser@'127.0.0.1' IDENTIFIED BY 'admin123';
GRANT ALL PRIVILEGES ON venteArme.* TO armeUser@'127.0.0.1';

DROP TABLE IF EXISTS produit;
CREATE TABLE produit (
    id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    nom VARCHAR(255) NOT NULL,
    imgURL VARCHAR(800) NOT NULL,
    prix INT NOT NULL,
    info VARCHAR(800) NOT NULL
);

INSERT INTO produit (nom, imgURL, prix, info) VALUES 
('pistolet', 'https://www.rivolier.com/media/catalog/product/cache/2524ae38bef0de0ce03f6f3bbb25c43c/g/i/gimc9black_01.jpg', 200, 'Pistolet calibre 9'),
('Sabre Lazer', 'https://skydreamer.fr/11214-home_default/star-wars-black-series-sabre-laser-rey-skywalker.jpg', 300, 'Idéal pour tout découper'),
('Machine Gun', 'https://vegasoutdooradventures.com/wp-content/uploads/2022/09/image-1.png', 1000, 'Une grosse mitrailleuse');

DROP TABLE IF EXISTS commande;
CREATE TABLE commande (
    id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    nom VARCHAR(255) NOT NULL,
    prenom VARCHAR(255) NOT NULL,
    factureRue VARCHAR(800) NOT NULL,
    factureCP int NOT NULL,
    factureVille VARCHAR(255) NOT NULL,
    livraisonRue VARCHAR(800) NOT NULL,
    livraisonCP int NOT NULL,
    livraisonVille VARCHAR(255) NOT NULL,
    dateCommande TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE panier_commande (
    id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    commandeID INT NOT NULL,
    produitID INT NOT NULL,
    quantite INT NOT NULL,
    numeroCommande VARCHAR(800),
    FOREIGN KEY (commandeID) REFERENCES commande(id),
    FOREIGN KEY (produitID) REFERENCES produit(id)
);
