<?php 
    session_start();
    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);
    error_reporting(E_ALL);

    $host = '127.0.0.1';
    $db   = 'venteArme';
    $user = 'armeUser';
    $pass = 'admin123'; 
    $pdo = new PDO("mysql:host=$host;dbname=$db;", $user, $pass);



    function createProduit($imgURL, $nom, $prix, $info){
        global$pdo;
        $req = $pdo->prepare("INSERT INTO produit (imgURL, nom, prix, info) values(?, ?, ?, ?);");
        $req->execute([$imgURL, $nom, $prix, $info]);
    };

    function readallproduit(){
        global $pdo;
        $req = $pdo->query("SELECT * from produit");
        return $req->fetchAll(); 
    };

    function readproduitbyid($id){
        global $pdo;
        $req = $pdo->prepare("SELECT * from produit where id =?");
        $req ->execute([$id]);
        return $req -> fetch();  
    };

    function updtproduit($imgURL, $nom, $prix, $info, $id){
        global $pdo;
        $req = $pdo->prepare("UPDATE produit SET imgURL =?, nom =?, prix =?, info =? where id =?");
        $req->execute($imgURL, $nom, $prix, $info, $id);
    };

    function deleteproduit($id){
        global $pdo;
        $req = $pdo->prepare("DELETE FROM produit WHERE id = ?");
        $req->execute([$id]);
    };

    function ajouterAuPanier($article, $quantite) {
        $_SESSION['panier'][] = array(
            'id' => $article['id'],
            'nom' => $article['nom'],
            'prix' => $article['prix'],
            'quantite' => $quantite
        );
    }

    function supprimerDuPanier($index) {
        if (isset($_SESSION['panier'][$index])) {
            unset($_SESSION['panier'][$index]);
            $_SESSION['panier'] = array_values($_SESSION['panier']); 
        }
    }

    function calculerMontantTotal() {
        $montantTotal = 0;
        foreach ($_SESSION['panier'] as $article) {
            $montantTotal += $article['prix'] * $article['quantite'];
        }
        return $montantTotal;
    }

function genererNumeroCommande() {
    $partieAleatoire = substr(str_shuffle("ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"), 0, 6);
    $horodatage = date("dmy");
    $numeroCommande = $partieAleatoire . $horodatage;
    return $numeroCommande;
}
function saveCommande($nom, $prenom, $factureRue, $factureCP, $factureVille, $livraisonRue, $livraisonCP, $livraisonVille){
    global $pdo;
    $req = $pdo->prepare("INSERT INTO commande (nom, prenom, factureRue, factureCP, factureVille, livraisonRue, livraisonCP, livraisonVille) values (?, ?, ?, ?, ?, ?, ?, ?);");
    $req->execute([$nom, $prenom, $factureRue, $factureCP, $factureVille, $livraisonRue, $livraisonCP, $livraisonVille]);
    return $pdo->lastInsertId();
}
function saveProduitDansCommande($commandeID, $produitID, $quantite, $numeroCommande){
    global $pdo;
    $req = $pdo->prepare("INSERT INTO panier_commande (commandeID, produitID, quantite, numeroCommande) values (?, ?, ?, ?);");
    $req->execute([$commandeID, $produitID, $quantite, $numeroCommande]);
}
function getAllCommandes() {
    global $pdo;
    $req = $pdo->query("SELECT * FROM commande");
    return $req->fetchAll();
}
function getProduitsCommande($commandeID) {
    global $pdo;
    $req = $pdo->prepare("SELECT pc.quantite, p.nom AS nom_produit, p.prix, c.nom AS nom_client, c.prenom
                        FROM panier_commande pc 
                        INNER JOIN produit p ON pc.produitID = p.id 
                        INNER JOIN commande c ON pc.commandeID = c.id
                        WHERE pc.commandeID = ?");
    $req->execute([$commandeID]);
    return $req->fetchAll();
}



?>