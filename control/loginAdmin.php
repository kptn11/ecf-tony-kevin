<?php
include_once "../model/data.php";
$login = $_POST["idAdmin"];
$pwd = $_POST["mdpAdmin"];

// Sélectionnez le mot de passe haché de la base de données en fonction du nom d'utilisateur
$req = $pdo->prepare("SELECT pwd FROM admin WHERE nom = ?");
$req->execute([$login]);
$adminData = $req->fetch();
if ($adminData && password_verify($pwd, $adminData["pwd"])) {
    $_SESSION["admin"] = true;
    header("Location: ../view/adminForm.php");
} else {
    echo "Mauvais login ou mot de passe";
}
?>
