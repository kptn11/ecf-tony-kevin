<?php
    include_once"../model/data.php";
    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
        // Récupérez les données du formulaire de paiement
        $nomCarte = $_POST['nom'];
        $token = $_POST['stripeToken'];
        require_once('../vendor/autoload.php');

        \Stripe\Stripe::setApiKey('sk_test_51NrIqRG9NQJGWnRGUxgduiCSqlrBDE28HxHo06c6BVkNRXCDkXflGo1Rd4gkw6m6NAwCkFULiGC9Uj8mHlyMJw6A00bBIIXdul');

    try {
        $montantTotal = calculerMontantTotal();
        $montantTotalEnCentimes = $montantTotal * 100;
        $charge = \Stripe\Charge::create([
            'amount' => $montantTotalEnCentimes, 
            'currency' => 'EUR',
            'source' => $token,
            'description' => 'Achat sur votre site',
        ]);

        echo "Paiement réussi !";
        unset($_SESSION['panier']);
        
    } catch (\Stripe\Exception\CardException $e) {
        echo "La carte a été refusée : " . $e->getError()->message;
    } catch (Exception $e) {
        echo "Erreur lors du paiement : " . $e->getMessage();
    }
    } else {
    header('Location: payer.php');
}
?>
