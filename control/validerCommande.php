<?php
    include_once "../model/data.php";
    $nom= $_POST["nom"];
    $prenom= $_POST["prenom"];
    $factureRue= $_POST["factureRue"];
    $factureCP= $_POST["factureCP"];
    $factureVille= $_POST["factureVille"];
    $livraisonRue= $_POST["livraisonRue"];
    $livraisonCP= $_POST["livraisonCP"];
    $livraisonVille= $_POST["livraisonVille"];

    $commandeID = saveCommande($nom, $prenom, $factureRue, $factureCP, $factureVille, $livraisonRue, $livraisonCP, $livraisonVille);
    $numeroCommande = genererNumeroCommande();
    foreach ($_SESSION['panier'] as $article) {
        $produitID = $article['id'];
        $quantite = $article['quantite'];
        saveProduitDansCommande($commandeID, $produitID, $quantite, $numeroCommande);
    };
?>
<h2>Votre commande est enregistrée</h2>
<div class="numCommande">N° de commande : <?= $numeroCommande ?></div>
<div class="recapPanier"><?php include "../view/miniPanier.php" ?></div>
<a href="../view/payer.php">Payer votre commande</a>