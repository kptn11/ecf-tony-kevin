<?php
include_once "../model/data.php"; 

$commandes = getAllCommandes();
?>

<!DOCTYPE html>
<html>
<head>
    <title>Liste des Commandes</title>
</head>
<body>
    <h1>Liste des Commandes</h1>

    <table>
        <tr>
            <th>ID Commande</th>
            <th>Nom Client</th>
            <th>Date de Commande</th>
            <th>Produits</th>
        </tr>

        <?php foreach ($commandes as $commande) { ?>
            <tr>
                <td><?php echo $commande['id']; ?></td>
                <td><?php echo $commande['nom'] . ' ' . $commande['prenom']; ?></td>
                <td><?php echo $commande['dateCommande']; ?></td>
                <td>
                    <?php
                    $produitsCommande = getProduitsCommande($commande['id']);

                    foreach ($produitsCommande as $produitCommande) {
                        echo $produitCommande['nom'] . ' x' . $produitCommande['quantite'] . '<br>';
                    }
                    ?>
                </td>
            </tr>
        <?php } ?>
    </table>

    <a href="index.php">Retour à l'accueil</a>
</body>
</html>
