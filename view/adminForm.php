<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>
    <?php include "partials/header.php"; ?>
    <h2>Ajouter un produit</h2>
    <form action="../control/createProduit.php" method="post">
        <div class="ajoutImgNom">
            <div>
                <label for="imgURL">Url de l'image du produit</label>
                <input name="imgURL" type="text">
            </div>
            <div>
                <label for="nom">Nom du produit</label>
                <input name="nom" type="text">
            </div>
        </div>
        <div>
            <label for="prix">Prix du produit</label>
            <input name="prix" type="number">
        </div>
        <div>
            <label for="info">Description du produit</label>
            <input name="info" type="textarea">
        </div>
        <input type="submit" name="ajouter" value="Ajouter">
    </form>
    
</body>
</html>