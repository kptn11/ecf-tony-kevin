<!DOCTYPE html>
<html>
<head>
    <title>Paiement</title>
    <script src="https://js.stripe.com/v3/"></script>
</head>
<body>
    <h1>Formulaire de Paiement</h1>
    <form method="POST" action="../control/traitementPaiement.php">
        <label for="nom">Nom sur la carte :</label>
        <input type="text" name="nom" required><br>

        <div id="card-element">
            <label for="numero_carte">Numéro de carte :</label>
            <input type="text" name="numero_carte" required><br>

            <label for="date_expiration">Date d'expiration (MM/AA) :</label>
            <input type="text" name="date_expiration" required><br>

            <label for="code_secu">Code de sécurité (CVV) :</label>
            <input type="text" name="code_secu" required><br>
        </div>
        <button id="payer-button" type="submit">Payer</button>
    </form>

    <a href="index.php">Retour à la boutique</a>
    <script>
        document.addEventListener("DOMContentLoaded", function () {
        var stripe = Stripe('pk_test_51NrIqRG9NQJGWnRGiFuaWz4bdkXPpPPOmkYEKnerBJOX4MOJFDnGcKUWzIZEOUjKbin0MXId9NZbpgSnPEg9oayn00LmhO81VH'); 

        var elements = stripe.elements();
        var cardElement = elements.create('card');

        cardElement.mount('#card-element');

        var form = document.querySelector('form');
        var paymentButton = document.getElementById('payer-button');

        form.addEventListener('submit', function (event) {
            event.preventDefault();

            stripe.createToken(cardElement).then(function (result) {
                if (result.error) {
                    // Gestion des erreurs lors de la création du jeton
                    console.error(result.error.message);
                } else {
                    // Utilisez result.token.id pour obtenir le jeton de paiement
                    var token = result.token.id;
                    // Transmettez ce jeton à votre script de traitement côté serveur
                    var hiddenInput = document.createElement('input');
                    hiddenInput.setAttribute('type', 'hidden');
                    hiddenInput.setAttribute('name', 'stripeToken');
                    hiddenInput.setAttribute('value', token);
                    form.appendChild(hiddenInput);

                    // Soumettez le formulaire
                    form.submit();
                }
            });
        });
    });

    </script>
</body>
</html>
