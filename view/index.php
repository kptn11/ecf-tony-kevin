<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>vente d'arme</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>
    <?php
        include '../model/data.php';
        include "partials/header.php";
    ?>
    <div class="produitCont">
        <?php
        if (isset($_POST['ajouter'])) {
            $articleId = $_POST['article_id'];
            $article = readproduitbyid($articleId); 
            $quantite = intval($_POST['quantite']);
            if (!isset($_SESSION['panier'])) {
                $_SESSION['panier'] = array(); 
            }

            // Ajoutez l'article au panier
            $article['quantite'] = $quantite;
            $_SESSION['panier'][] = $article;
            header("Location: index.php");
            exit;
}
            $mesInfos = readallproduit();
        foreach($mesInfos as $data){ 
        ?>
            <div class="produit">
                <div>
                    <img class="produitIMG" src="<?= $data["imgURL"]?>" alt="">
                    <div>
                        <div class="nom"><?= $data["nom"]?></div>
                        <div class="prix"><?= $data["prix"]?>€</div>
                    </div>
                </div>
                <div class="description"><?= $data["info"]?></div>
                <div class="ajouter">
                    <form method="POST">
                        <input type="hidden" name="article_id" value="<?= $data["id"] ?>">
                        <input class="qt" type="number" name="quantite" value="1" step="1">
                        <button type="submit" name="ajouter">Ajouter</button>
                    </form>
                </div>
                <?php 
                    if($_SESSION["admin"]){
                ?>
                    <a href="../control/deleteProduit.php?id=<?= $data["id"]; ?>">Effacer</a>
                <?php } ?>
            </div>
        <?php } ?>
    </div>
</body>
</html>