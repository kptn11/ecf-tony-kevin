<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>
    <?php 
        include "../model/data.php";
        include "partials/header.php";
    ?>
    <form action="../control/loginAdmin.php" method="post">
        <label for="idAdmin">Identifiant</label>
        <input name="idAdmin" type="text">
        <label for="mdpAdmin">Mot de passe</label>
        <input name="mdpAdmin" type="text">
        <input type="submit" value="Continuer">
    </form>
</body>
</html>