<?php
include_once "../model/data.php";
?>
<body>
    <h1>Votre Panier</h1>
    <?php if (empty($_SESSION['panier'])) { ?>
        <p>Votre panier est vide.</p>
    <?php } else { ?>
        <table>
            <tr>
                <th>Nom</th>
                <th>Prix</th>
                <th>Quantité</th>
                <th>Total</th>
            </tr>
            <?php foreach ($_SESSION['panier'] as $index => $article) { ?>
                <tr>
                    <td><?= $article['nom'] ?></td>
                    <td><?= $article['prix'] ?></td>
                    <td><?= $article['quantite'] ?></td>
                    <td><?= $article['prix'] * $article['quantite'] ?></td>
                </tr>
            <?php } ?>
            <tr>
                <td colspan="3">Montant total :</td>
                <td><?= calculerMontantTotal() ?></td>
            </tr>
        </table>
    <?php } ?>