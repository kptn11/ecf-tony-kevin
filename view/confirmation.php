
<!DOCTYPE html>
<html>
<head>
    <title>Confirmation de commande</title>
</head>
<body>
    <div class="container">
    <h1>Confirmation de commande</h1>
    <form action="../control/validerCommande.php" method="POST">
        <label for="nom">Nom :</label>
        <input type="text" name="nom" required><br>

        <label for="prenom">Prénom :</label>
        <input type="text" name="prenom" required><br>

        <label for="factureRue">Adresse de facturation :</label>
        <input type="text" name="factureRue" required><br>

        <label for="factureCP">Code postal de facturation :</label>
        <input type="text" name="factureCP" required><br>

        <label for="factureVille">Ville de facturation :</label>
        <input type="text" name="factureVille" required><br>

        <label for="livraisonRue">Adresse de livraison :</label>
        <input type="text" name="livraisonRue" required><br>

        <label for="livraisonCP">Code postal de livraison :</label>
        <input type="text" name="livraisonCP" required><br>

        <label for="livraisonVille">Ville de livraison :</label>
        <input type="text" name="livraisonVille" required><br>

        <button type="submit">Confirmer la commande</button>
    </form>

    <a href="index.php">Retour à la boutique</a>
    </div>
    <style>
        body {
            font-family: Arial, sans-serif;
            background-color: #f0f0f0;
            margin: 0;
            padding: 0;
        }

        .container {
            max-width: 800px;
            margin: 0 auto;
            padding: 20px;
        }

        h1 {
            color: rgb(55, 0, 255); /* Couleur principale */
        }

        form {
            margin-top: 20px;
        }

        label {
            display: block;
            margin-bottom: 5px;
            font-weight: bold;
        }

        input[type="text"] {
            width: 100%;
            padding: 10px;
            margin-bottom: 10px;
            border: 1px solid #ccc;
            border-radius: 5px;
        }

        button[type="submit"] {
            background-color: rgb(55, 0, 255); /* Couleur principale */
            color: white;
            padding: 10px 20px;
            border: none;
            border-radius: 5px;
            cursor: pointer;
        }

        button[type="submit"]:hover {
            background-color: #3700FF; /* Couleur principale au survol */
        }

        a {
            display: block;
            text-decoration: none;
            color: rgb(55, 0, 255); /* Couleur principale pour les liens */
            margin-top: 10px;
        }
    </style>
</body>
</html>