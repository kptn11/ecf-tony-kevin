<?php
    include_once "../model/data.php";
?>
<header>
    <a href="index.php">
        <img src="../img/Logo_Arme.png" alt="Logo">
    </a>
    <a href="index.php" style="text-decoration: none; color: black;">
    <h1>Vente d'armes</h1>
    </a>
    <div id="panier">
        <a href="panier.php">
            <img src="../img/Logo_Panier.png" alt="panier">
        </a>
        <div id= "panier-popOver" >
            <?php 
                include "miniPanier.php"
            ?>
        </div>
    </div>
    <?php
        if($_SESSION["admin"]){ ?>
            <div class="partieAdmin" style="display: flex; flex-direction: column;">
                <a href="adminForm.php">Ajouter produits</a>
                <a href="voirCommande.php">Voir les commandes</a>
                <a href="../control/logoutAdmin.php">Déconnexion</a>
            </div>
    <?php } ?>
</header>