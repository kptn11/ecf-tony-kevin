<?php
include_once "../model/data.php";

if (!isset($_SESSION['panier'])) {
    $_SESSION['panier'] = array(); 
}
?>

<!DOCTYPE html>
<html>
<head>
    <title>Panier</title>
</head>
<body>
    <h1>Votre Panier</h1>
    <div class="container">
    <?php if (empty($_SESSION['panier'])) { ?>
        <p>Votre panier est vide.</p>
    <?php } else { ?>
        <table>
            <tr>
                <th>Nom</th>
                <th>Prix</th>
                <th>Quantité</th>
                <th>Total</th>
                <th>Action</th>
            </tr>
            <?php foreach ($_SESSION['panier'] as $index => $article) { ?>
                <tr>
                    <td><?= $article['nom'] ?></td>
                    <td><?= $article['prix'] ?>€</td>
                    <td><?= $article['quantite'] ?></td>
                    <td><?= $article['prix'] * $article['quantite'] ?>€</td>
                    <td><a href="panier.php?action=supprimer&index=<?= $index ?>">Supprimer</a></td>
                </tr>
            <?php } ?>
            <tr>
                <td colspan="3">Montant total :</td>
                <td><?= calculerMontantTotal() ?>€</td>
            </tr>
        </table>
    <?php } ?>
    </div>
    <div class="retour-valider">
        <a href="index.php"><- Retour à la boutique</a>
        <a href="confirmation.php">Valider mon panier -></a>
    </div>
    <?php
    // Gestion de la suppression d'un article du panier
    if (isset($_GET['action']) && $_GET['action'] === 'supprimer' && isset($_GET['index'])) {
        $index = intval($_GET['index']);
        supprimerDuPanier($index);
        header('Location: panier.php');
    }
    ?>
    <style>
        body {
            margin: 0;
            padding: 0;
        }

        .container {
            max-width: 800px;
            margin: 0 auto;
            padding: 20px;
        }

        h1 {
            color: rgb(55, 0, 255); 
            text-align: center;
        }

        table {
            width: 100%;
            border-collapse: collapse;
            margin-top: 20px;
        }

        table, th, td {
            border: 1px solid #ccc;
        }

        th, td {
            padding: 10px;
            text-align: center;
        }

        th {
            background-color: #735bff; 
            color: white;
        }

        tr:nth-child(even) {
            background-color: #f2f2f2; /* Couleur de fond pour les lignes paires */
        }

        a {
            text-decoration: none;
            color: rgb(55, 0, 255); /* Couleur principale pour les liens */
        }

        a:hover {
            text-decoration: underline; /* Ajouter un soulignement au survol des liens */
        }

        /* Media query pour la mise en page responsive */
        @media (max-width: 768px) {
            table {
                font-size: 14px; /* Réduire la taille de police pour les petits écrans */
            }
        }

        .retour-valider{
            width:100%;
            display: flex;
            justify-content: space-around;
        }

    </style>
</body>
</html>
